#!/bin/sh

echo
echo "       -=- ejabberd pre uninstallation script -=- "
echo "              (c) 2005-2012 ProcessOne "
echo 
echo "* Stopping ejabberd instance"
/Applications/ejabberd-2.1.11/bin/ejabberdctl stop 
/Applications/ejabberd-2.1.11/bin/ejabberdctl stopped
/Applications/ejabberd-2.1.11/bin/epmd -names | grep -q name || {
  echo "* Stopping Erlang Portmapper Deamon"
  /Applications/ejabberd-2.1.11/bin/epmd -kill
}
echo "* backup current config"
cp -r /Applications/ejabberd-2.1.11/conf /Applications/ejabberd-2.1.11/conf.$(date +%y%m%d)
echo 
echo "==> Pre uninstallation tasks finished"
